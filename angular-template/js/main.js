/**
 * @author Denise Nordlöf
 */

/**
 * Main AngularJS Web Application
 */
var app = angular.module('tutorialWebApp', [
  'ngRoute'
]);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "partials/home.html", controller: "PageCtrl"})
    // Pages
    .when("/all", {templateUrl: "partials/all.html", controller: "PageCtrl"})
    .when("/images", {templateUrl: "partials/images.html", controller: "PageCtrl"})
    .when("/albumView", {templateUrl: "partials/albumView.html", controller: "PageCtrl"})
    .when("/imageEdit", {templateUrl: "partials/imageEdit.html", controller: "imageEditCtrl"})
    .when("/about", {templateUrl: "partials/about.html", controller: "PageCtrl"})
    .when("/contact", {templateUrl: "partials/contact.html", controller: "PageCtrl"})
    .when("/manage", {templateUrl: "partials/manage.html", controller: "PageCtrl"})
    // else 404
    .otherwise({redirectTo : '/'});
}]);

/**
 * A template for controller to create dynamic data lter on!
 */
app.controller('PageCtrl', function (/* $scope, $location, $http */) {
  console.log("PageCtrl loaded successfully");
});

app.controller('imageEditCtrl', function (/* $scope, $location, $http */) {
  console.log("imageEditCtrl loaded successfully");
  $("#tokenlist-loaded").tokenfield();
});