README
-----------------
Author: Denise Nordlöf
Last updated: 18th May 2016

This is actualy the skeleton for a site created in the web interaction course at KTH
-----------------

VIEWING THE SITE
-----------------
To actually view the website with all angular working it has to be run on a server. The easiest way to do this on a Mac is to use a python server on localhost. You do this by standing in the folder of the project and then writing the following command in the terminal:
python -m SimpleHTTPServer

This requires that you have python installed on your computer of course. Probably you have it included if you are a Mac user. After the command has been executed successfully you can then acces all files via http://localhost:8000