/**
* @author Denise Nordlöf & Anna Olsson
*/

var app = angular.module('app', [
  'ngRoute'
  ]);

var loggedIn;
var database = firebase.database();
var storageRef = storage.ref();
var username;

firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    loggedIn = firebase.auth().currentUser;
    username = loggedIn.uid;
  } else {
    loggedIn = null;
    username = null;
  }
});

/**
* Configure the Routes
*/
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    // Startpage
    .when("/", {templateUrl: "partials/home.html", controller: "MyImagesCtrl"})
    // Pages
    .when("/albums", {templateUrl: "partials/albums.html", controller: "AlbumsCtrl"})
    .when("/myImages", {templateUrl: "partials/myImages.html", controller: "MyImagesCtrl"})
    .when("/albumView", {templateUrl: "partials/albumView.html", controller: "AlbumViewCtrl"})
    .when("/imageEdit", {templateUrl: "partials/imageEdit.html", controller: "ImageEditCtrl"})
    .when("/login", {templateUrl: "partials/login.html", controller: "LoginCtrl"})
    .when("/home", {templateUrl: "partials/home.html", controller: "PageCtrl"})
    // else if 404 - page not found
    .otherwise({redirectTo : '/'});
  }]);


/**
* PLACEHOLDER CONTROLLER
* Controller used for placeholder.html
*/
app.controller('PageCtrl', function (/*$scope, $location, $http */) {
  console.log("PageCtrl loaded successfully");
}); //End of PageCtrl


/**
* CONTROLLER
* Controller used for albumView.html
*/
app.controller('AlbumViewCtrl', function (/*$scope, $location, $http */) {
  console.log("AlbumView loaded successfully");
}); //End of AlbumViewCtrl


/**
* CONTROLLER
* Controller used for myImages.html
*/
app.controller('MyImagesCtrl', function ($scope, $rootScope, $location/*, $http */) {
  console.log("MyImages loaded successfully");
  

  $scope.getUserPhotos = function(){
    database.ref('/users/' + username ).once('value').then(function(photos) {
      $rootScope.userPhotos = [];

      photos.forEach(function(photo) {
        if($rootScope.userPhotos.indexOf(photo) == -1){
          var photo = photo.val();
          var photoUrl = photo.full_storage_uri;
          $rootScope.userPhotos.push(photo);
          $rootScope.$digest();
        }
      });    
    });

    $scope.loadDetailsForPhoto = function(photoKey) {
      $rootScope.getCurrentPhotoObject(photoKey, function() {
        $location.path('/imageEdit');
      });
    }
  }

}); //End of MyImagesCtrl


/**
* CONTROLLER
* Controller used for albums.html
*/
app.controller('AlbumsCtrl', function ($scope ,$rootScope, $location/*, $http */) {
  console.log("AlbumsCtrl loaded successfully");

  $scope.getAllTags = function(){
    database.ref('/tags/').once('value').then(function(photos) {
      $rootScope.tagNames = [];

      photos.forEach(function(tag){
        $rootScope.tagNames.push(tag.val());
      });
      $scope.$digest();
    });
  }

  //Gets all photo objects for a certain tag and puts it in $scope value
  $rootScope.getTagPhotos = function(tagName){
    $rootScope.tagPhotos = []; //empty
    database.ref('/tags/' + tagName).once('value').then(function(tagObject){
      $rootScope.tagPhotos = tagObject.val().photos;
      $rootScope.$digest();

    });
  }

}); //End of AlbumsCtrl


/**
* CONTROLLER
* Controller for header.html
*/
app.controller('HeaderCtrl', function ($scope , $location, $rootScope/*, $http */) {
  console.log("HeaderCtrl loaded successfully");

  //Logging out the user and redirecting to login page
  $scope.logout=function(){
    auth.signOut().then(function(result) {
      $scope.$apply(function() {
        $rootScope.currentUser = "No user signed in";
        loggedIn = false;
        $location.path('/login');  
      });

    }).catch(function(error) {
      console.log("error: " + error);
    });
  };

  //Redirects the user to login page if not logged in
  $scope.$watch(function() { 
    return $location.path(); 
  }, function(newValue, oldValue){  
    if (loggedIn == null && newValue != '/login'){  
      $location.path('/login');  
    }  
  });

  $scope.uploadNewPhoto = function(event) {
    // Start the pic file upload to Firebase Storage.

    var photo = event.target.files[0];
    var photoRef = storageRef.child("/photos/" + photo.name);
    var metadata = {
      contentType: photo.type
    };
    var photoUploadTask = photoRef.put(photo, metadata);
    var photoCompleter = new $.Deferred();
    photoUploadTask.on('state_changed', null, error => {
      photoCompleter.reject(error);
      console.error('Error while uploading new photo', error);
    }, () => {
      console.log('New photo uploaded. Size:', photoUploadTask.snapshot.totalBytes, 'bytes.');
      var url = photoUploadTask.snapshot.metadata.downloadURLs[0];
      console.log('File available at', url);
      photoCompleter.resolve(url);
    });


    return photoCompleter.promise().then(url => {
      var newPhotoKey = database.ref('/users/' + username).push().key;
      var photoKey = newPhotoKey;

      var postData = {
        photoKey : photoKey,
        full_url: url,
        timestamp: firebase.database.ServerValue.TIMESTAMP,
        full_storage_uri: photoRef.toString(),
        tags: {
          0: "dummyTag"}
        };
        var updates = {};
        updates['/users/' + username +'/'+ newPhotoKey] = postData;


        database.ref().update(updates).then(() => newPhotoKey);
      });
  }

  $rootScope.getCurrentPhotoObject = function(photoKey, callback) {
    database.ref('/users/' + username + '/' + photoKey).once('value').then(function(photos) {
      $rootScope.currentPhotoObject = photos.val();
      $rootScope.$digest();

      if (callback) {
        callback();
      }
    });
  }

}); //End of HeaderCtrl


/**
* CONTROLLER
* Controller for imageEdit.html
*/
app.controller('ImageEditCtrl', function ($scope, $rootScope /* , $location, $http */) {
  console.log("ImageEditCtrl loaded successfully");

  $scope.addPhotoToTag = function(tagName){
    var photo = $rootScope.currentPhotoObject;

    if(photo.tags && photo.tags.indexOf(photo) != -1){
      return;
    }

    database.ref('/tags/' + tagName).once('value').then(function(tagObject){
      var tagValue = tagObject.val();
      var tagPhotos = [];
      if (tagValue) {
        tagPhotos = tagValue.photos;
      }

      tagPhotos.push(photo);
      database.ref('/tags/' + tagName).update({
        name: tagName,
        photos : tagPhotos
      });
    });

    //Update photosObject
    database.ref('/users/' + username + '/' + $rootScope.currentPhotoObject.photoKey).once('value').then(function(photoObject){
      var photoTags = photoObject.val().tags;

      if(photoTags.indexOf(tagName) == -1){
        photoTags.push(tagName);
        database.ref('/users/' + username + '/' + $rootScope.currentPhotoObject.photoKey).update({
          tags: photoTags
        });
      } 
      else{
        console.log("Your photo is already tagged with this tag");
      }

      $rootScope.getCurrentPhotoObject($rootScope.currentPhotoObject.photoKey);
    }); 

  }


  $scope.removeTag = function(tagName){
    //remove photo from tag
    database.ref('/tags/' + tagName).once('value').then(function(tagObject){
      var tagValue = tagObject.val();
      if (!tagValue || !tagValue.photos) {
        return;
      }
      
      var tagPhotos = tagValue.photos;

      tagPhotos.forEach(function(photo){
        var tagPhotoKey = $rootScope.currentPhotoObject.photoKey;
        
        if(photo.photoKey == tagPhotoKey){
          var index = tagPhotos.indexOf(photo);
          tagPhotos.splice(index, 1);
        }

      });

      if(tagPhotos.length == 0){
        database.ref('/tags/' + tagName).remove();  
      }else{
        database.ref('/tags/' + tagName).update({
          photos: tagPhotos
        });
      }
      console.log($rootScope.currentPhotoObject.photoKey +"has been removed from" + tagName);
    });

    //remove tag from photo
    database.ref('/users/' + username + '/' + $rootScope.currentPhotoObject.photoKey).once('value').then(function(userObject){
      var photoTags = userObject.val().tags;

      var index = photoTags.indexOf(tagName);
      photoTags.splice(index, 1);



      database.ref('/users/' + username + '/' + $rootScope.currentPhotoObject.photoKey).update({
        tags: photoTags
      });

      console.log(tagName +"has been removed from" + $rootScope.currentPhotoObject.photoKey);
      $rootScope.getCurrentPhotoObject($rootScope.currentPhotoObject.photoKey);
    });
  }

}); //End of ImageEditCtrl


/**
* CONTROLLER
* Controller for login.html
*/
app.controller('LoginCtrl', function ($scope, $rootScope , $location/*, $http */) {
  console.log("LoginCtrl loaded successfully");

  //Create new user
  $scope.createNewUser=function(username, password) {
    auth.createUserWithEmailAndPassword(username, password).catch(function(error) {
      console.log('error: ' + error);
    });
  };

  //Login user
  $scope.login=function(username, password) {
    auth.signInWithEmailAndPassword(username, password).then(function(result) {

      $scope.$apply(function() {
        $rootScope.currentUser = result.email;
        loggedIn = true;
        $location.path('/home');  
      });

    }).catch(function(error) {
      console.log(error);
    });
  };
}); //End of LoginCtrl